async function getResponse() {
  let response = await fetch("https://api.myjson.com/bins/p2dnz");
  let content = await response.json();

  let list = document.querySelector(".posts");
  let desc = document.querySelector(".box");
  let key;

  for (key in content) {
    list.innerHTML += `
            <div class="post__item">
              <div class="post__wrapper">
                <div class="post__title">
                  <div class="progress"></div>
                  <h4>${content[key].title}</h4>
                  <h5>${content[key].year}</h5>
                </div>
                <div class="play__wrapper js-videoWrapper" data-youtube="MgZXt8L4bkM">
                  <img class="post__img" src="${content[key].poster}" width="300">
                  <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowTransparency="true" allowfullscreen data-src="https://www.youtube.com/embed/MgZXt8L4bkM?autoplay=1&modestbranding=1&rel=0&hl=ru&showinfo=0&color=white"></iframe>
                  <div class="play__youtube videoPoster" data-url=""></div>
                </div>
                
                <div class="post__rate">
                  <a href="#" class="post__share"><?xml version="1.0" encoding="utf-8"?>
                  <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                     viewBox="0 0 34.9 31.3" style="enable-background:new 0 0 34.9 31.3;" xml:space="preserve">
                  <path fill="#707070" d="M25,20c-1.7,0-3.1,0.7-4.2,1.8L13,18.2c0.1-0.5,0.2-1,0.2-1.6c0-0.6-0.1-1.1-0.2-1.6l12-5.6
                    c1,1.1,2.5,1.8,4.2,1.8c3.1,0,5.7-2.5,5.7-5.7c0-3.1-2.5-5.7-5.7-5.7c-3.1,0-5.7,2.5-5.7,5.7c0,0.1,0,0.3,0,0.4l-12.4,5.7
                    C10,10.7,8.4,10,6.6,10C3,10,0,13,0,16.7c0,3.7,3,6.6,6.6,6.6c1.8,0,3.3-0.7,4.5-1.8l8.2,3.8c0,0.1,0,0.3,0,0.4
                    c0,3.1,2.5,5.7,5.7,5.7c3.1,0,5.7-2.5,5.7-5.7C30.7,22.5,28.1,20,25,20z"/>
                  </svg>
                  </a>
                  <div class="post__comment post__box">${content[key].comments_count}
                    <div class="comment__img post__box-img"><img src="./img/comment.svg" alt=""></div>
                  </div>
                  <div class="post__rate post__box">${content[key].rank||content[key].expectations_count}
                    ${
      
                        ((content[key].rank||content[key].expectations_count) !== 0) ? `<svg
                        class="progress-ring"
                        width="30"
                        height="30">
                        <circle
                        class="progress-ring__circle"
                        stroke="#707070"
                        stroke-width="3"
                        fill="transparent"
                        data-rateValue=${content[key].rank||content[key].expectations_count}
                        r="10"
                        cx="15"
                        cy="15"/>
                        </svg>` : `<div class="rate__img post__box-img"><img src="./img/rate.svg" alt=""></div>`
                      }

                  </div>
                  <div class="post__like post__box">${content[key].likes_count}
                    <div class="like__img post__box-img"><img src="./img/Like.svg" alt=""></div>
                  </div>
                </div>
              </div>
            </div>
        `
        desc.innerHTML += `
            <div class="popular__box">
                <img class="box__img "src="${content[key].poster}" alt="">
                <div class="rating">
                ${
      
                  ((content[key].rank||content[key].expectations_count) !== 0) ? `<svg
                  class="progress-ring"
                  width="120"
                  height="120">
                  <circle
                  class="progress-ring__circle"
                  stroke="#22CA71"
                  stroke-width="6"
                  fill="transparent"
                  data-rateValue=${content[key].rank||content[key].expectations_count}
                  r="32"
                  cx="60"
                  cy="60"/>
                  </svg>` : `<div class="rate__img post__box-img"><img src="./img/rate.svg" alt=""></div>`
              }
                </div>
                <div class="film__info">
                  <a href="#" class="film__title">${content[key].title}</a>
                  <div class="box__info">
                      <div class="box__year">${content[key].year}</div>
                      <div class="box__director">Director: ${content[key].director}</div>
                      <div class="box__writer">Writer: ${content[key].writer}</div>
                  </div>
                  <p class="box__content">${content[key].content}</p>
                <div class="box__wrapper">
                  <div class="box__rate">
                    <a href="#" class="popular__share">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                      viewBox="0 0 34.9 31.3" style="enable-background:new 0 0 34.9 31.3;" xml:space="preserve">
                    <path fill="#707070" d="M25,20c-1.7,0-3.1,0.7-4.2,1.8L13,18.2c0.1-0.5,0.2-1,0.2-1.6c0-0.6-0.1-1.1-0.2-1.6l12-5.6
                      c1,1.1,2.5,1.8,4.2,1.8c3.1,0,5.7-2.5,5.7-5.7c0-3.1-2.5-5.7-5.7-5.7c-3.1,0-5.7,2.5-5.7,5.7c0,0.1,0,0.3,0,0.4l-12.4,5.7
                      C10,10.7,8.4,10,6.6,10C3,10,0,13,0,16.7c0,3.7,3,6.6,6.6,6.6c1.8,0,3.3-0.7,4.5-1.8l8.2,3.8c0,0.1,0,0.3,0,0.4
                      c0,3.1,2.5,5.7,5.7,5.7c3.1,0,5.7-2.5,5.7-5.7C30.7,22.5,28.1,20,25,20z"/>
                    </svg>
                    </a>
                    <div class="popular__comment popular__item">${content[key].comments_count}
                      <div class-"comment__img  post__box-img"><img src="./img/comment.svg" alt=""></div>
                    </div>
                    <div class="popular__rate popular__item">${content[key].rank||content[key].expectations_count}
                    ${
        
                      ((content[key].rank||content[key].expectations_count) !== 0) ? `<svg
                      class="progress-ring"
                      width="30"
                      height="30">
                      <circle
                      class="progress-ring__circle"
                      stroke="#707070"
                      stroke-width="3"
                      fill="transparent"
                      data-rateValue=${content[key].rank||content[key].expectations_count}
                      r="10"
                      cx="15"
                      cy="15"/>
                      </svg>` : `<div class="rate__img post__box-img"><img src="./img/rate.svg" alt=""></div>`
                  }
                    </div>
                    <div class="popular__like popular__item">${content[key].likes_count}
                      <div class-"like__img  post__box-img"><img src="./img/Like.svg" alt=""></div>
                    </div>
                  </div>
                  <div class="read__more"><a href="#">Read more</a></div>
                </div>
            </div>
        `

  }
  
  $(document).ready(function() {
    $(".posts").slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      arrows: false,
      verticalSwiping: false,
      dots: false,
      autoplay: false,
    });
  function vertical() {
    return {
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      vertical:true,
      verticalSwiping: true,
      dots: false,
      autoplay: false,
    }
  }
  function horizontal() {
    return {
      slidesToShow: 4,
      slidesToScroll: 1,
      draggable:true,
      arrows: false,
      vertical:false,
      verticalSwiping: false,
      dots: false,
      autoplay: false,
    }
  }
$('#ver').on('click', function(){
  event.preventDefault();
  $(".posts").slick("destroy");
  $(".posts").slick(vertical());
})
$('#hor').on('click', function(){
  event.preventDefault();
  $(".posts").slick("destroy");
  $(".posts").slick(horizontal());
})
});
$(document).ready(function() {

  var circle = document.querySelectorAll('circle');
  circle.forEach(function(el) {
    var radius = el.r.baseVal.value;
    var circumference = radius * 2 * Math.PI;

    el.style.strokeDasharray = `${circumference} ${circumference}`;
    el.style.strokeDashoffset = `${circumference}`;

    function setProgress(percent) {
      const offset = circumference - percent / 10 * circumference;
      el.style.strokeDashoffset = offset;
    }

    const rate = parseInt(el.dataset.ratevalue);


    setProgress(rate);
  });



  $(".box").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    vertical:true,
    verticalSwiping: true,
    dots: false,
    autoplay: false,
  });

  function popVertical() {
    return {
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      vertical:true,
      verticalSwiping: true,
      dots: false,
      autoplay: false,
    }
  }
  function popHorizontal() {
    return {
      slidesToShow: 1,
      slidesToScroll: 1,
      draggable:true,
      arrows: false,
      vertical:false,
      verticalSwiping: false,
      dots: false,
      autoplay: false,
    }
  }
  $('#horz').on('click', function(){
     event.preventDefault();
      $(".box").slick("destroy");
      $(".box").slick(popVertical());
    })
  $('#verz').on('click', function(){
      event.preventDefault();
      $(".box").slick("destroy");
      $(".box").slick(popHorizontal());
    })

  });


  $(document).on('click','.play__youtube',function(e) {
    //отменяем стандартное действие button
    e.preventDefault();
    var poster = $(this);
    // ищем родителя ближайшего по классу
    var wrapper = poster.closest('.js-videoWrapper');
    videoPlay(wrapper);
  });
  
  //вопроизводим видео, при этом скрывая постер
  function videoPlay(wrapper) {
    var iframe = wrapper.find('.js-videoIframe');
    // Берем ссылку видео из data
    var src = iframe.data('src');
    // скрываем постер
    wrapper.addClass('videoWrapperActive');
    // подставляем в src параметр из data
    iframe.attr('src',src);
  }

 
}

getResponse();



